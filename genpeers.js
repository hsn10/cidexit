import * as https from 'https'
import { promises as fs } from 'fs'

const PEERS = 'https://raw.githubusercontent.com/web3-storage/web3.storage/main/PEERS'
const OUTPUT = 'peering.cmd'

function loadPeers() {
   return new Promise( (resolve, reject) => {
   https.get(PEERS, (res) => {
      if (res.statusCode !== 200) {
          const err = `Did not get an OK from the server. Code: ${res.statusCode}`
          res.resume();
          reject(err);
          return;
      }
      let data = '';
      res.on('data', (chunk) => {
         data += chunk;
      });

      res.on('close', () => {
          console.log('Retrieved all data');
          resolve(data)
      });
   }
   )
 })
}

function parsePeering(data) {
   let result = []
   for (const line of data.split('\n') ) {
      const i = line.lastIndexOf('/')
      if ( i > 3 )
      {
         const id = line.substring(i+1)
         result.push(id)
      }
   }
  return result
}

function savePeering(nodes) {
  fs.open(OUTPUT,'w+').then( (handle) => {
     for (const node of nodes) {
        handle.write(`ipfs swarm connect /ipfs/${node}\r\n`)
     }
     handle.close()
  })
}

async function main() {
   const data = await loadPeers()
   const nodes = parsePeering(data)
   console.log(nodes)
   savePeering(nodes)
}

main()
