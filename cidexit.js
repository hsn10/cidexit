/**
 * Creates tree with objects stored at web3.storage
 * @license MIT
 */
import { Web3Storage } from 'web3.storage'
import { create } from 'ipfs-http-client'

const TARGET='/cidexit/'

const ipfs = await create()

function getAccessToken() {
   const ENV_TOKEN = 'WEB3STORAGE_TOKEN'
   const token = process.env[ENV_TOKEN]
   if (!token) {
                  console.error("Environment variable "+ENV_TOKEN+" is not set")
                  process.exit(2)
   }
   return token
}

function makeStorageClient () {
   return new Web3Storage( { token: getAccessToken() } )
}

function linkDataset(name, cid) {
   const path = TARGET+name
   ipfs.files.rm(path, {recursive: true, flush: false}).catch( (err) => {}).then(
     (r2) => ipfs.files.cp('/ipfs/'+cid, path, { flush: true, parents: true }) ).catch( (err) =>
       console.error(`Error fetching ${name} / ${cid}`))
}

async function main() {
   const storage = makeStorageClient()
   
   for await (const upload of storage.list()) {
      console.log(`${upload.name} - cid: ${upload.cid} - size: ${upload.dagSize}`)
      linkDataset(upload.name, upload.cid)
   }
   
}

main()

   